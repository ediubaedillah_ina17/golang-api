package v1

import (
	"fmt"
	"golang-api/dto"
	"golang-api/helper"
	"golang-api/service"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
)

type ProductHandler interface {
	CreateProduct(ctx *gin.Context)
	All(ctx *gin.Context)
	FindOneProductByID(ctx *gin.Context)
	UpdateProduct(ctx *gin.Context)
	DeleteProduct(ctx *gin.Context)
}

type productHandler struct{
	productService service.ProductService
	jwtService service.JWTService
}

func NewProductHandler(productService service.ProductService, jwtService service.JWTService) ProductHandler {
	return &productHandler{
		productService: productService,
		jwtService: jwtService,
	}
}

func (c *productHandler) CreateProduct(ctx *gin.Context) {
	var createProductRequest dto.CreateProductRequest
	err := ctx.ShouldBind(&createProductRequest)

	if err != nil {
		response := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	authHeader := ctx.GetHeader("Authorization")
	token := c.jwtService.ValidateToken(authHeader, ctx)
	claims := token.Claims.(jwt.MapClaims)
	userID := fmt.Sprintf("%v", claims["user_id"])

	res, err := c.productService.CreateProduct(createProductRequest, userID)
	if err != nil {
		response := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusUnprocessableEntity, response)
		return
	}

	response := helper.BuildResponse(true, "OK", res)
	ctx.JSON(http.StatusCreated, response)
}

func (c *productHandler) All(ctx *gin.Context) {
	authHeader := ctx.GetHeader("Authorization")
	token := c.jwtService.ValidateToken(authHeader, ctx)
	claims := token.Claims.(jwt.MapClaims)
	userID := fmt.Sprintf("%v", claims["user_id"])

	products, err := c.productService.All(userID)
	if err != nil {
		response := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	response := helper.BuildResponse(true, "OK", products)
	ctx.JSON(http.StatusOK, response)
}

func (c *productHandler) FindOneProductByID(ctx *gin.Context) {
	id := ctx.Param("id")

	res, err := c.productService.FindOneProductByID(id)
	if err != nil {
		response := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	response := helper.BuildResponse(true, "OK", res)
	ctx.JSON(http.StatusOK, response)
}

func (c *productHandler) UpdateProduct(ctx *gin.Context) {
	updateProductRequest := dto.UpdateProductRequest{}
	err := ctx.ShouldBind(&updateProductRequest)

	if err != nil {
		response := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	authHeader := ctx.GetHeader("Authorization")
	token := c.jwtService.ValidateToken(authHeader, ctx)
	claims := token.Claims.(jwt.MapClaims)
	userID := fmt.Sprintf("%v", claims["user_id"])
	fmt.Print("User ID", userID)
	id, _ := strconv.ParseInt(ctx.Param("id"), 0, 64)
	updateProductRequest.ID = id
	product, err := c.productService.UpdateProduct(updateProductRequest, userID)

	if err != nil {
		response := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusUnprocessableEntity, response)
		return
	}

	response := helper.BuildResponse(true, "OK", product)
	ctx.JSON(http.StatusOK, response)
}

func (c *productHandler) DeleteProduct(ctx *gin.Context) {
	id := ctx.Param("id")

	authHeader := ctx.GetHeader("Authorization")
	token := c.jwtService.ValidateToken(authHeader, ctx)
	claims := token.Claims.(jwt.MapClaims)
	userID := fmt.Sprintf("%v", claims["user_id"])

	err := c.productService.DeleteProduct(id, userID)
	if err != nil {
		response := helper.BuildErrorResponse("Failed to process request", err.Error(), helper.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	response := helper.BuildResponse(true, "OK!", helper.EmptyObj{})
	ctx.JSON(http.StatusOK, response)
}