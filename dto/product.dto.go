package dto

type CreateProductRequest struct {
	Name  string `json:"name" form:"name" binding:"required,min=1"`
	Price int64  `json:"price" form:"price" binding:"required"`
}

type UpdateProductRequest struct {
	ID    int64  `json:"id" form:"id"`
	Name  string `json:"name" binding:"required,min=1"`
	Price int64  `json:"price" binding:"required"`
}