package main

import (
	"golang-api/config"
	v1 "golang-api/handler/v1"
	"golang-api/middleware"
	"golang-api/repository"
	"golang-api/service"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

var (
	db *gorm.DB = config.SetupDatabaseConnection()
	userRepository repository.UserRepository = repository.NewUserRepository(db)
	ProductRepository repository.ProductRepository = repository.NewProductRepo(db)
	jwtService service.JWTService = service.NewJWTService()
	authService service.AuthService = service.NewAuthService(userRepository)
	userService service.UserService = service.NewUserService(userRepository)
	productService service.ProductService = service.NewProductService(ProductRepository)
	authHandler v1.AuthHandler = v1.NewAuthHandler(authService, jwtService, userService)
	userHandler v1.UserHandler = v1.NewUserHandler(userService, jwtService)
	productHandler v1.ProductHandler = v1.NewProductHandler(productService, jwtService)
)

func main() {
	defer config.CloseDatabaseConnection(db)
	r := gin.Default()
	
	r.GET("/ping", func (ctx *gin.Context)  {
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Welcome to Golang API",
		})
	})

	authRoutes := r.Group("api/auth")
	{
		authRoutes.POST("/login", authHandler.Login)
		authRoutes.POST("/register", authHandler.Register)
	}

	userRoutes := r.Group("api/user", middleware.AuthorizeJWT(jwtService))
	{
		userRoutes.GET("/profile", userHandler.Profile)
		userRoutes.PUT("/update", userHandler.Update)
	}

	productRoutes := r.Group("api/product", middleware.AuthorizeJWT(jwtService))
	{
		productRoutes.GET("/", productHandler.All)
		productRoutes.GET("/:id", productHandler.FindOneProductByID)
		productRoutes.POST("/create", productHandler.CreateProduct)
		productRoutes.PUT("/update/:id", productHandler.UpdateProduct)
		productRoutes.DELETE("/delete/:id", productHandler.DeleteProduct)
	}

	r.Run(":8888")
}