package entity

type Product struct {
	ID     int64  `json:"id" gorm:"primary_key:auto_increment"`
	Name   string `json:"name" gorm:"type:varchar(255);not null"`
	Price  int64  `json:"price" gorm:"type:bigint;not null"`
	UserID int64  `gorm:"type:bigint(20);" json:"-"`
	User   User   `gorm:"foreignkey:UserID;constraint:onUpdate:CASCADE,onDelete:CASCADE" json:"user"`
}