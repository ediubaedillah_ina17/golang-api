package entity

type User struct {
	ID       int64  `gorm:"primary_key:auto_increment" json:"id"`
	Name     string `gorm:"type:varchar(255);not null" json:"name"`
	Email    string `gorm:"uniqueIndex;type:varchar(255);unique;not null" json:"email"`
	Password string `gorm:"type:varchar(255);not null" json:"-"`
	// Password string `gorm:"->;<-;not null" json:"-"` ->;<- type long text
	// Token    string `gorm:"-" json:"token,omitempty"`
}