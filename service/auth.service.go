package service

import (
	"errors"
	"golang-api/repository"
	"log"

	"golang.org/x/crypto/bcrypt"
)

type AuthService interface {
	VerifyCredential(email string, password string) error
}

type authService struct {
	userRepository repository.UserRepository
}

func NewAuthService(userRep repository.UserRepository) AuthService {
	return &authService {
		userRepository: userRep,
	}
}

func (service *authService) VerifyCredential(email string, password string) error {
	user, err := service.userRepository.FindByEmail(email)
	if err != nil {
		println("hehe")
		println(err.Error())
		return err
	}
	isValidPassword := comparePassword(user.Password, []byte(password))
	if !isValidPassword {
		return errors.New("Failed to login, check your credential!")
	}
	return nil
}

func comparePassword(hashedPwd string, plaiPassword []byte) bool {
	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, plaiPassword)
	if err != nil {
		log.Println(err)
		return false
	}

	return true
}